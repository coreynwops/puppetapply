# Puppet Serverless Catalog Compilation

## WTF

This is a POC of a serverless app that takes facts and code and churns out a catalog in JSON format. 

This catalog is then to be consumed by `puppet apply` on the node that requested the catalog. 

This is meant to run in a serverless runtime or in a container. 

## Why
Since this is intended for Serverless we can scale puppet catalog compilation in great numbers.  Suggested usage would 
be on Openwisk.

https://medium.com/openwhisk/ruby-goes-serverless-apache-openwhisk-adds-native-support-for-ruby-7134faee14e

or

https://faastruby.io/  

## Serverless test

### Create a body.json file
The first step will create the body.json file but we need to add additional key value pars. 
1. `puppet facts | jq '.values' > body.json`
2. encapsulate the facts in a another hash `{"facts": { ... }}`
2. add a code entrypoint that includes some other code  (ie. `include roles::webserver`)

Have a look at the body.json file in this repo.


## Usage
To run the container `docker run -ti --rm nwops/puppetapply`
`
or

`curl -s -X POST -d code="cron{'stroopwaffletime': ensure => present, minute => '10,30', hour => '*'}" https://api.faastruby.io/my-workspace2/example`

Which should spit out

```json
{
  "catalog": {
    "tags": [
      "settings",
      "stdlib",
      "stdlib::stages",
      "stages",
      "class"
    ],
    "name": "81c23849d2af.domain",
    "version": 1537564613,
    "code_id": null,
    "catalog_uuid": "4a7cd6ba-aecb-45c4-8deb-f61b442e81d0",
    "catalog_format": 1,
    "environment": "tmp",
    "resources": [
      {
        "type": "Stage",
        "title": "main",
        "tags": [
          "stage"
        ],
        "exported": false,
        "parameters": {
          "name": "main"
        }
      },
      {
        "type": "Class",
        "title": "Settings",
        "tags": [
          "class",
          "settings"
        ],
        "exported": false
      },
      {
        "type": "Class",
        "title": "main",
        "tags": [
          "class"
        ],
        "exported": false,
        "parameters": {
          "name": "main"
        }
      },
      {
        "type": "Class",
        "title": "Stdlib",
        "tags": [
          "class",
          "stdlib"
        ],
        "exported": false
      },
      {
        "type": "Class",
        "title": "Stdlib::Stages",
        "tags": [
          "class",
          "stdlib::stages",
          "stdlib",
          "stages"
        ],
        "exported": false
      },
      {
        "type": "Stage",
        "title": "setup",
        "tags": [
          "stage",
          "setup"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 35,
        "exported": false,
        "parameters": {
          "before": "Stage[main]"
        }
      },
      {
        "type": "Stage",
        "title": "runtime",
        "tags": [
          "stage",
          "runtime"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 36,
        "exported": false,
        "parameters": {
          "require": "Stage[main]"
        }
      },
      {
        "type": "Stage",
        "title": "setup_infra",
        "tags": [
          "stage",
          "setup_infra"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 37,
        "exported": false
      },
      {
        "type": "Stage",
        "title": "deploy_infra",
        "tags": [
          "stage",
          "deploy_infra"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 38,
        "exported": false
      },
      {
        "type": "Stage",
        "title": "setup_app",
        "tags": [
          "stage",
          "setup_app"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 39,
        "exported": false
      },
      {
        "type": "Stage",
        "title": "deploy_app",
        "tags": [
          "stage",
          "deploy_app"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 40,
        "exported": false
      },
      {
        "type": "Stage",
        "title": "deploy",
        "tags": [
          "stage",
          "deploy"
        ],
        "file": "/app/example/modules/stdlib/manifests/stages.pp",
        "line": 41,
        "exported": false
      }
    ],
    "edges": [
      {
        "source": "Stage[main]",
        "target": "Class[Settings]"
      },
      {
        "source": "Stage[main]",
        "target": "Class[main]"
      },
      {
        "source": "Stage[main]",
        "target": "Class[Stdlib]"
      },
      {
        "source": "Stage[main]",
        "target": "Class[Stdlib::Stages]"
      }
    ],
    "classes": [
      "settings",
      "stdlib",
      "stdlib::stages"
    ]
  }
}


```

## Compiling the container

To build the container: `docker build -t nwops/puppetapply .`

## Running curl to get the catalog and plugins


`curl -X POST  https://api.faastruby.io/my-workspace2/example -d @body.json -H "Content-Type: application/json"`

or 

`curl -s -X POST -d code="cron{'stroopwaffletime': ensure => present, minute => '10,30', hour => '*'}" https://api.faastruby.io/my-workspace2/example`


or

To use docker just run `docker run -ti --rm nwops/puppetapply`

## Adding modules
To add more puppet modules just add to the Puppetfile and run `librarian-puppet install`. 


## Using with Puppet Apply

Since the catalog is passed in JSON format back to the user.  Additionally, we also pass the required plugin files as well.  These will need to be
synced to the file system before the catalog can be applied.

I have created a quick script to perform all this magic called `get_n_apply.rb`. Again this is just a POC.

This script will send a a request and apply the catalog.

## TODO 
1. return in msgpack format to save bandwith and increase speed
2. find all puppet:/// urls and change them in the code?  (There is no puppet server right now that implements a file server)

## Known issues
* Contains only a small set of puppet modules and supports very few resources
* Doesn't allow to pass in top scope variables unless in site.pp or initial code

## Deploying function
1. push code to master after setting initial workspace

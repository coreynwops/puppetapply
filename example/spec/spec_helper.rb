# frozen_string_literal: true

require 'bundler/setup'
require 'pry'
require 'rspec'

def fixtures_dir
  File.join(File.dirname(__FILE__), 'fixtures')
end

# mock class of something that looks like the event passed in
class HttpEvent
  attr_reader :body, :query_params, :headers
  def initialize(opts = {})
    @body = opts[:body]
    @query_params = opts[:query_params]
    @headers = opts[:headers]
  end
end


RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

end

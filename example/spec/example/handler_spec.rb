require_relative '../../handler'
require 'spec_helper'

RSpec.describe :handle do

  let(:event) do
    HttpEvent.new(body: File.read(File.join(fixtures_dir, 'body.json')))
  end

  let(:response) do
    handler(event)
  end

  let(:parsed_response) do
    JSON.parse(response)
  end

  describe :event do
    it '#returns string' do
      expect(response).to be_a String
    end

    it '#returns JSON hash' do
      expect(parsed_response).to be_a Hash
    end

    it '#returns data with catalog as a parsable catalog' do
      expect(parsed_response['catalog']).to be_a Hash
    end

    it '#returns data with catalog with plugins' do
      expect(parsed_response['plugins']).to be_a Hash
    end

    it '#plugins has files as keys' do
      expect(parsed_response['plugins'].keys).to eq ["/cron_core/lib/puppet/type/cron.rb",
                                                     "/cron_core/lib/puppet/provider/cron/crontab.rb"]
    end
  end



end

#
# require 'msgpack'
#
# puts MessagePack.unpack(response)
#

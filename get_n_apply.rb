#!/usr/bin/env ruby
# Author: Corey Osman
# email: corey@nwops.io
# Purpose: POC of applying a catalog that was compiled in a serverless api endpoint

require 'puppet'
require 'json'
require 'tmpdir'
require 'net/http'
require 'uri'
require 'puppet/configurer'

# @return [Hash] - get the current facts of the node running this script
def facts
  begin
    Puppet.initialize_settings
    Puppet.initialize_facts
    Puppet::Node::Facts.indirection.find(Puppet[:node_name_value])
  rescue Puppet::DevError
    # do nothing
  end
end

def request_body
  {
      code: "file{'/tmp/papplytest': ensure => present, content => 'Hello from serverless puppet'}",
      facts: facts
  }
end

def retrieve_catalog
  uri = URI.parse("https://api.faastruby.io/my-workspace2/example")
  request = Net::HTTP::Post.new(uri)
  request.content_type = "application/json"
  request.body = request_body.to_json
  req_options = {
      use_ssl: uri.scheme == "https",
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end

  if response.code == '200'
    JSON.parse(response.body)
  else
    puts "Response was #{response.code}"
    puts JSON.parse(response.body)
    exit 1
  end

end

def create_plugin_files(plugins, modules_path)
  plugins.each do |path, content|
    file = File.join(modules_path, path)
    FileUtils.mkdir_p(File.dirname(file))
    puts "Creating plugin file #{file}"
    File.write(file, content)
  end
end

def read_catalog(text)
  format = Puppet::Resource::Catalog.default_format
  begin
    catalog = Puppet::Resource::Catalog.convert_from(format, text)
  rescue StandardError => detail
    raise Puppet::Error, format(_("Could not deserialize catalog from %{format}: %{detail}"), format: format, detail: detail), detail.backtrace
  end

  catalog.to_ral
end

def run
  data = retrieve_catalog
  output = nil
  catalog = data['catalog']
  plugins = data['plugins']
  Dir.mktmpdir do |dir|
    modules_path = File.join(dir, 'modules')
    Puppet.settings[:basemodulepath] = Array(modules_path)
    create_plugin_files(plugins, modules_path)
    env = Puppet.lookup(:environments).get(Puppet[:environment])
    Puppet.override(current_environment: env, loaders: Puppet::Pops::Loaders.new(env)) do
      catalog = read_catalog(catalog.to_json)
      configurer = Puppet::Configurer.new
      output = configurer.run(catalog: catalog, pluginsync: false)
    end
  end
  output
end

puts run
